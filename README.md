# About
Our entry in the real-time occupancy tracking challenge for make-zurich 2018.
  
Links:
http://platform.makezurich.ch  
http://now.makezurich.ch/project/36  
  
# Solution Approaches
Some ideas how we could determin the current occupacy rate and what to do with this information.  
Some of these ideas could also be combined to improve the measurements.  
Also not sure about legal constraints.  

## People Detection
How the presence of people is detected and counted.  

### Computer vision
Analyze video footage with OpenCV to detect and count people.  
To ensure privacy, the video material needs to be processed and analyzed in real-time without any interactions with external systems.  
  
https://www.pyimagesearch.com/2015/11/09/pedestrian-detection-opencv/  
  
Questions:  
* how are the videos stored
* how can they be accessed securely
  
### Mobile Counter
Capture mobile network packages and count unique subscribers that are present during the ride. 
Probably not accurate as the IMSI is not sent out frequently.
  
https://github.com/Oros42/IMSI-catcher  
https://www.gsma.com/newsroom/wp-content/uploads//TS.06-v14.0-1.pdf  

### WiFI Sniffer
Use a cheap ESP8266 chip to capture probe requests from mobile phones. Count unique MAC adresses that are present for a certain amount of time to determin if the device is actually inside the vehicle.  
Not really accurate, as some people might have their WiFi turned off. Nevertheless, could still be useful information.

### Bluetooth Sniffer
Quite similar to the WiFi sniffer, except that other hardware is needed. When implemented on a ESP32, this can be easily combined with the WiFi sniffer.  
Also not very accurate.  

## Data Propagation
After the measurements have been collected, the data is propagated to another network for further processing.  
Only the current count of commuters asossiated with time and place is transmitted.  
  
### Method
#### lorem
wifi, cell, rf, integrated?  
#### LoRaWAN
needs to be checked if data transmission on that scale complies with the regulated duty cycle limits.
probably not
